----------------------------------------------------------------------------------

-- TRAITOR
--Copyright (C)  2019 - INRIA

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description: Testbench for configuration_manager

--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.triggy_common.all;
use work.common.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity config_manager_tb is
end config_manager_tb;

architecture Behavioral of config_manager_tb is

    procedure wait_clocks(signal clk : in std_logic; n : in integer) is
        begin
            for i in 1 to n loop
                wait until falling_edge(clk);
            end loop;
        end procedure;

    constant T : time := 20 ns;
    signal clk: std_logic := '0';
    signal reset: std_logic;

    component configuration_manager is
        port(clk:       in std_logic;
             reset:     in std_logic;

             --fifo from Uart PC rx (cross clock domains)
             din:         in byte;
             din_rdy:     in std_logic;
             din_read:    out std_logic;-- send read order

             --fifo to Uart PC tx (cross clock domains)
             dout:        out byte;
             dout_write:  out std_logic;-- send write order

            -- triggers
            trig_amplitude :  out std_logic_vector(7 downto 0);
            trig_confs:       out TrigConfs
             );
    end component;

    component fifo IS
        PORT (
        clk : IN STD_LOGIC;
        rst : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        wr_rst_busy : OUT STD_LOGIC;
        rd_en : IN STD_LOGIC;
        rd_rst_busy : OUT STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC
        );
    end component;

    -- fifo rx
  signal fifo_rx_dout: std_logic_vector(7 downto 0);
  signal fifo_rx_available, fifo_rx_empty, fifo_rx_read_order: std_logic;

  -- fifo tx
  signal fifo_tx_dout, fifo_tx_din: std_logic_vector(7 downto 0);
  signal fifo_tx_empty, fifo_tx_write_order, fifo_tx_read: std_logic;

  signal trig_confs: TrigConfs;

  signal pc_rx_data: std_logic_vector(7 downto 0);
  signal pc_rx_write : std_logic;

begin

    clk <= not clk after T/2;


    -- reset = 1 for first clock cycle and then 0
    reset <= '1', '0' after T/2;

    pc_uart_rx_fifo: fifo
        port map(
        rst           => reset,
        clk           => clk,
        din           => pc_rx_data,
        wr_en         => pc_rx_write,
        rd_en         => fifo_rx_read_order,
        dout          => fifo_rx_dout,
        rd_rst_busy   => open,
        wr_rst_busy   => open,
        full          => open,
        empty         => fifo_rx_empty
        );

      fifo_rx_available <= not(fifo_rx_empty);

    pc_uart_tx_fifo: fifo
        port map(
        rst           => reset,
        clk           => clk,
        din           => fifo_tx_din,
        wr_en         => fifo_tx_write_order,
        rd_en         => fifo_tx_read,
        dout          => fifo_tx_dout,
        rd_rst_busy   => open,
        wr_rst_busy   => open,
        full          => open,
        empty         => fifo_tx_empty
        );

    dut: configuration_manager
    port map (
        clk         => clk,
        reset       => reset,

        din         => fifo_rx_dout,
        din_rdy     => fifo_rx_available,
        din_read    => fifo_rx_read_order,

        dout        => fifo_tx_din,
        dout_write  => fifo_tx_write_order,

        trig_amplitude => open,
        trig_confs     => trig_confs
    );

    fifo_tx_read <= '0';

    -- now we send data to fifo_rx: 00 00 07 (read at address 00 07)

    test: process
    begin
        pc_rx_data  <= x"00";
        pc_rx_write   <= '0';

        wait_clocks(clk, 5);
        -- push 00 to fifo rx
        pc_rx_data  <= x"00";
        pc_rx_write   <= '1';

        wait_clocks(clk, 1);
        --push 00 again

        -- we repeat so we do not change the signals

        wait_clocks(clk, 1);
        -- push 07
        pc_rx_data <= x"07";

        wait_clocks(clk, 1);
        -- stop pushing
        pc_rx_write <= '0';

        -- wait end of simu
        wait_clocks(clk, 10);

    end process;


end Behavioral;
