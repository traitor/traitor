----------------------------------------------------------------------------------

-- TRAITOR
--Copyright (C)  2019 - INRIA

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: L. Claudepierre
-- Target Devices: ArtiX7
-- Description: Test-bench for TRAITOR

--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.triggy_common.all;
use work.common.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity traitor_tb is
  Port (
        clk_100 : out STD_LOGIC;
        Dswitch : out STD_LOGIC_VECTOR(1 downto 0);
        stm_trig: out STD_LOGIC;
        trig_delay_in: out trig_delays;
        trig_width_in : out  trig_widths;
        trig_amplitude_in : out  STD_LOGIC_VECTOR(7 downto 0);

        -- OUPUT--
        led     : in STD_LOGIC_VECTOR (15 downto 0);
        Fault_trig_out : in STD_LOGIC;
        clk_init : in STD_LOGIC;
        clk_dphase : in STD_LOGIC;
        clk_XOR_out : in STD_LOGIC;
        clk_fault_out : in STD_LOGIC;
        clk_fault_out2 : in STD_LOGIC );
end traitor_tb;

architecture Behavioral of traitor_tb is

    --// input to the glitchy module
    signal s_clk_100 : STD_LOGIC:='0';
    signal s_Dswitch : STD_LOGIC_VECTOR(1 downto 0):="00";
    signal s_stm_trig : STD_LOGIC:='0';
    signal s_delay : trig_delays;
    signal s_width : trig_widths;
    signal s_amplitude :  STD_LOGIC_VECTOR(7 downto 0):=x"e0";

    --// output from the glitchy module
    signal s_led : STD_LOGIC_VECTOR (15 downto 0):=x"0000";
    signal s_Fault_trig_out : STD_LOGIC;
    signal s_clk_init : STD_LOGIC;
    signal s_clk_dphase : STD_LOGIC;
    signal s_clk_XOR_out : STD_LOGIC;
    signal s_clk_fault_out : STD_LOGIC;
    signal s_clk_fault_out2 : STD_LOGIC;

    signal s_rst : STD_LOGIC :='0';
    constant T100 : time := 10 ns;
    constant Trst : time := 500 ns;
    constant Twait : time := 100 ns;
begin

UUT : entity work.mmcm_reset
port map (
    clk_100 => s_clk_100,
    Dswitch => s_Dswitch,
    stm_trig => s_stm_trig,
    trig_delay_in =>s_delay ,
    trig_width_in=>s_width ,
    trig_amplitude_in=> s_amplitude,

    -- OUPUT--
    led   => s_led,
    Fault_trig_out => s_Fault_trig_out ,
    clk_init => s_clk_init ,
    clk_dphase=> s_clk_dphase,
    clk_XOR_out => s_clk_XOR_out,
    clk_fault_out => s_clk_fault_out,
    clk_fault_out2 => s_clk_fault_out2
);

tb_clock100 : process
        begin
            s_clk_100 <= '0';
            wait for T100/2;
            s_clk_100 <= '1';
            wait for T100/2;
        end process;

s_stm_trig <= '0', '1' after 1*T100+0.25*T100+Trst;
s_rst <= '0', '1' after 5*T100, '0' after 7*T100;

remplissage_trigconf : process(s_rst)
begin
    if rising_edge(s_rst) then
        s_delay(0) <= x"00000007";
        s_width(0) <= x"00000005";
        for i in 1 to NB_TRIGGERS-1 loop
            s_delay(i) <= x"00000000";
            s_width(i) <= x"00000000";
        end loop;
    end if;
end process;



end Behavioral;
