----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description:  communication with UART interface

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity uart is
  port(clk:  in std_logic;
       reset:     in std_logic;
       uart_conf: in UartConf;

       rx:        in std_logic;--rx wire
       tx:        out std_logic;--tx wire

       tx_data:   in std_logic_vector(7 downto 0);--data byte to write
       rx_data:   out std_logic_vector(7 downto 0);--data byte read

       rx_rdy:    out std_logic;--a byte is ready to be read
       tx_rdy:    out std_logic;--a byte can be written
       tx_start:  in std_logic;--write a byte, must last 1 clk

       error_flag:  out std_logic;

       baud_clk_o:  out std_logic;
       tick_o:      out std_logic
       );
end uart;

architecture behavior of uart is

  component baud_gen is
    port(main_clk:  in std_logic;
         reset:     in std_logic;
         baud_clk:  out std_logic;--generated signal
         --tot for divider is 2^32
         uart_conf:  in UartConf);--step for divider
  end component;

  component uart_tx is
    port(baud_clk:  in std_logic;
         reset:     in std_logic;
         uart_conf: in UartConf;

         tx:        out std_logic;--rx wire
         tx_rdy:    out std_logic;-- 1 when we can send a new byte

         tx_data:   in std_logic_vector(7 downto 0);--data byte read
         tx_start:  in std_logic; -- must last 1 tick
         tick_o:    out std_logic
         );
  end component;

  component uart_rx is
    port(baud_clk:    in std_logic;
         reset:       in std_logic;
         uart_conf:   in UartConf;
         rx:          in std_logic;--rx wire
         rx_data:     out std_logic_vector(7 downto 0);--data byte read
         rx_rdy:      out std_logic;
         error_flag:  out std_logic;
         tick_o:    out std_logic
         );
  end component;

  component pulser is
    port(clk_in:       in std_logic;
         clk_out:       in std_logic;
         reset:     in std_logic;
         pulse_in:  in std_logic;
         pulse_out: out std_logic
         );
  end component;

  signal baud_clk:        std_logic;
  signal tx_start_pulse:  std_logic;
  signal tx_data_buf:     std_logic_vector(7 downto 0);

  begin

    baud_clk_o <= baud_clk;

    process(clk, reset)
    begin
      if reset = '1' then
        tx_data_buf <= X"00";
      elsif rising_edge(clk) then
        if tx_start = '1' then
          tx_data_buf <= tx_data;
        end if;
      end if;
    end process;

    gen: baud_gen port map (
      main_clk    => clk,
      reset       => reset,
      baud_clk    => baud_clk,
      uart_conf   => uart_conf
    );

    tx_pulser: pulser
      port map(
        clk_in      => clk,
        clk_out     => baud_clk,
        reset       => reset,
        pulse_in    => tx_start,
        pulse_out   => tx_start_pulse
      );
    -- tick_o <= tx_start_pulse;

    tx_component: uart_tx port map(
      baud_clk      => baud_clk,
      reset         => reset,
      uart_conf     => uart_conf,
      tx            => tx,
      tx_data       => tx_data_buf,
      tx_rdy        => tx_rdy,
      tx_start      => tx_start_pulse,
      tick_o        => open
    );

    rx_component: uart_rx port map(
      baud_clk      => baud_clk,
      reset         => reset,
      uart_conf     => uart_conf,
      rx            => rx,
      rx_data       => rx_data,
      rx_rdy        => rx_rdy,
      error_flag    => error_flag,
      tick_o        => tick_o
    );


end behavior;
