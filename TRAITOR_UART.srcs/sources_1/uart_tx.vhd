----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description: 

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity uart_tx is
  port(baud_clk:  in std_logic;
       reset:     in std_logic;
       uart_conf: in UartConf;

       tx:        out std_logic;--rx wire
       tx_rdy:    out std_logic;-- 1 when we can send a new byte

       tx_data:   in std_logic_vector(7 downto 0);--data byte read, must be constant while transmitting
       tx_start:  in std_logic; -- must last 1 baud_clk
       tick_o:    out std_logic
       );
end uart_tx;

architecture behavior of uart_tx is


  --IDLE: waiting for start bit
  --START: writing start bit(s)
  --DATA: writing data bits
  --PARITY: writing parity bit (or not)
  --STOP: writing stop bit(s)
  type FSM_STATE is (IDLE, START1, START2, DATA0, DATA1, DATA2, DATA3, DATA4, DATA5, DATA6, DATA7, PARITY, STOP1, STOP2);

  signal state: FSM_STATE;
  signal tick: std_logic; -- RE for each bit

  signal tx_data_transmit: std_logic_vector(7 downto 0);
  signal start_flag: std_logic;
  signal datafill: std_logic;
  signal running: std_logic;
  signal end_flag: std_logic;

  signal baud_clk_count: unsigned(3 downto 0);--OVERSAMPLING = 16
  signal baud_clk_tick_value: unsigned(3 downto 0);-- tick when this value is reached

  signal data_parity: std_logic;

  signal ready: std_logic;

  begin

    --start and ready
    -- start_flag <= tx_start and ready;--start flag
    ready <= not(running);

    tx_rdy <= ready;

    tick_o <= tick;

    start_up: process(reset, baud_clk)
      begin
        if reset = '1' then
          start_flag <= '0';
        elsif rising_edge(baud_clk) then
          if ready = '1' and start_flag = '0' and tx_start = '1' then
            start_flag <= '1';
          elsif ready = '0' then
            start_flag <= '0';
          end if;
        end if;
    end process;

    running_up: process(reset, baud_clk)
      begin
        if reset = '1' then
          running <= '0';
        elsif rising_edge(baud_clk) then
          if start_flag = '1' then
            running <= '1';
          elsif end_flag = '1' then
            running <= '0';
          end if;
        end if;
    end process;

    -- baud clk counter
    count_baud_clk: process(reset, baud_clk)
      begin
        if reset = '1' then
          baud_clk_count <= (others => '0');
        elsif rising_edge(baud_clk) then
          if running = '1' then
            baud_clk_count <= baud_clk_count + 1;
          end if;
        end if;
    end process;

    baud_clk_tick_value_update: process(reset, baud_clk)
      begin
        if reset = '1' then
          baud_clk_tick_value <= X"0";
        elsif rising_edge(baud_clk) then
          if start_flag = '1' then
            baud_clk_tick_value <= baud_clk_count + 1;
          end if;
        end if;
    end process;

    -- tick generation
    tick_gen: process(reset, baud_clk)
      begin
        if reset = '1' then
          tick <= '0';
        elsif rising_edge(baud_clk) then
          if baud_clk_count = baud_clk_tick_value and running = '1' then
            tick <= '1';
          else
            tick <= '0';
          end if;
        end if;
    end process;

    --parity
    data_parity <= tx_data(7) xor tx_data(6) xor tx_data(5) xor tx_data(4) xor tx_data(3) xor tx_data(2) xor tx_data(1) xor tx_data(0);

    -- set tx (we loose one baud_clk per frame)
    tx <= not uart_conf.idle_polarity when state = START1 else
          tx_data_transmit(0) when datafill = '1' else
          data_parity when state = PARITY and uart_conf.parity = Even else
          not data_parity when state = PARITY and uart_conf.parity = Odd else
          uart_conf.idle_polarity;

    tx_transmit: process(reset, baud_clk)
      begin
        if reset = '1' then
          tx_data_transmit <= (others => '0');
        elsif rising_edge(baud_clk) then
          if tick = '1' then
            if state = START1 then
              tx_data_transmit <= tx_data;
            elsif datafill = '1' then
              tx_data_transmit <= tx_data_transmit(0) & tx_data_transmit(7 downto 1);
            end if;
          end if;
        end if;
    end process;

    -- FSM state transition
    transition: process(reset, baud_clk)
      begin
        if reset = '1' then
          state <= IDLE;
          datafill <= '0';
        elsif rising_edge(baud_clk) then
          trans: case state is

            when IDLE =>
              datafill <= '0';
              end_flag <= '0';
              if running = '1' and tick = '1' then
                state <= START1;
              else
                state <= IDLE;
              end if;

            when START1 => --START1 wait for start bit(s)

              if tick = '1' then
                if uart_conf.start_bits = One then
                  state <= DATA0;
                  datafill <= '1';
                else
                  state <= START2;
                end if;
              else
                state <= START1;
              end if;

            when START2 =>
              if tick = '1' then
                state <= DATA0;
                datafill <= '1';
              else
                state <= START2;
              end if;

            when DATA0 =>
              if tick = '1' then
                state <= DATA1;
              else
                state <= DATA0;
              end if;

            when DATA1 =>
              if tick = '1' then
                state <= DATA2;
              else
                state <= DATA1;
              end if;

            when DATA2 =>
              if tick = '1' then
                state <= DATA3;
              else
                state <= DATA2;
              end if;

            when DATA3 =>
              if tick = '1' then
                state <= DATA4;
              else
                state <= DATA3;
              end if;

            when DATA4 =>
              if tick = '1' then
                state <= DATA5;
              else
                state <= DATA4;
              end if;

            when DATA5 =>
              if tick = '1' then
                state <= DATA6;
              else
                state <= DATA5;
              end if;

            when DATA6 =>
              if tick = '1' then
                  if uart_conf.data_bits = Seven then
                    datafill <= '0';
                    if uart_conf.parity = None then
                      state <= STOP1;
                    else
                      state <= PARITY;
                    end if;
                  else
                    state <= DATA7;
                  end if;
                else
                  state <= DATA6;
                end if;

            when DATA7 =>
              if tick = '1' then
                datafill <= '0';
                if uart_conf.parity = None then
                  state <= STOP1;
                else
                  state <= PARITY;
                end if;
              else
                state <= DATA7;
              end if;

            when PARITY =>
              if tick = '1' then
                state <= STOP1;
              else
                state <= PARITY;
              end if;

            when STOP1 =>
              if tick = '1' then
                if uart_conf.stop_bits = One then
                  state <= IDLE;
                  end_flag <= '1';
                else
                  state <= STOP2;
                end if;
              else
                state <= STOP1;
              end if;

            when STOP2 =>
              if tick = '1' then
                state <= IDLE;
                end_flag <= '1';
              else
                state <= STOP2;
              end if;
          end case;
        end if;
    end process;

end behavior;
