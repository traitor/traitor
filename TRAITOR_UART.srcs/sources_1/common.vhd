----------------------------------------------------------------------------------

-- TRAITOR
--Copyright (C)  2019 - INRIA

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description: Constant and global data

--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package common is

  constant OVERSAMPLING: unsigned(4 downto 0) := "10000"; --16

  subtype byte is std_logic_vector(7 downto 0);
  subtype u8 is unsigned(7 downto 0);
  subtype u16 is unsigned(15 downto 0);
  subtype u32 is unsigned(31 downto 0);
  subtype u64 is unsigned(63 downto 0);

  type Parity is (None, Even, Odd);

  type StopBits is (One, Two);

  type StartBits is (One, Two);

  type DataBits is (Seven, Eight);

  type UartConf is
    record
      idle_polarity:      std_logic;
      baud_gen_step:      unsigned(31 downto 0);-- step = round( baud rate * oversampling * 2^32 / clock frequency )
      start_bits:         StartBits;
      stop_bits:          StopBits;
      parity:             Parity;
      data_bits:          DataBits;
  end record;

  function parse_databits(db : std_logic) return DataBits;
  function parse_parity(p : std_logic_vector(1 downto 0)) return Parity;
  function parse_stopbits(sb : std_logic) return StopBits;
  function parse_startbits(sb : std_logic) return StartBits;

end common;

package body common is
  function parse_databits(db : std_logic) return DataBits is
  begin
    if db = '1' then
      return Seven;
    else
      return Eight;
    end if;
  end parse_databits;

  function parse_parity(p : std_logic_vector(1 downto 0)) return Parity is
  begin
    case p is
      when "00" => return None;
      when "01" => return Odd;
      when "10" => return Even;
      when others => return None;
    end case;
  end parse_parity;

  function parse_stopbits(sb : std_logic) return StopBits is
  begin
    if sb = '1' then
      return Two;
    else
      return One;
    end if;
  end parse_stopbits;

  function parse_startbits(sb : std_logic) return StartBits is
  begin
    if sb = '1' then
      return Two;
    else
      return One;
    end if;
  end parse_startbits;
end common;
