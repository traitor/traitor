----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description:  Fifo autoreader

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

-- generate a pulse of one clock cycle on rising_edge of pulse_in
entity fifo_autoreader is
  port(clk:             in std_logic;
       reset:           in std_logic;
       enable:          in std_logic;
       fifo_empty:      in std_logic;
       fifo_data:       in std_logic_vector(7 downto 0);
       fifo_read_order: out std_logic;

       data:            out std_logic_vector(7 downto 0);
       data_en:         out std_logic
       );
end fifo_autoreader;

architecture behavior of fifo_autoreader is

  signal fifo_read_order_buf: std_logic;
  signal wait_sig: std_logic;

  begin

    fifo_read_order <= fifo_read_order_buf;
    -- data_en <= wait_sig;

    process(clk, reset)-- read from tx_fifo to send to PC
      begin
        if reset = '1' then
          fifo_read_order_buf <= '0';
        elsif rising_edge(clk) then
          if fifo_empty = '0' and enable = '1' and fifo_read_order_buf = '0' then -- we have data and we can send it
            fifo_read_order_buf <= '1';
          else
            fifo_read_order_buf <= '0';
          end if;
        end if;
    end process;

    process(clk, reset)-- read from tx_fifo to send to PC, next clock cycle data go from fifo to uart
      begin
        if reset = '1' then
          data <= (others => '0');
          data_en <= '0';
          wait_sig <= '0';
        elsif rising_edge(clk) then
          if wait_sig = '1' then
            data <= fifo_data;
            data_en <= '1';
          else
            data_en <= '0';
          end if;
          if fifo_read_order_buf = '1' then
            wait_sig <= '1';
          else
            wait_sig <= '0';
          end if;
        end if;
    end process;

end behavior;
