----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description: 

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity baud_gen is
  port(main_clk:  in std_logic;
       reset:     in std_logic;
       baud_clk:  out std_logic;--generated signal
       --tot for divider is 2^32
       uart_conf:  in UartConf);--step for divider
end baud_gen;

architecture behavior of baud_gen is

  signal acc: unsigned(32 downto 0);--at each main_clk RE, step is added to acc. When acc reaches tot (=2^32), generate a RE on baud_clk

  begin

    process(main_clk, reset)
    begin
      if reset = '1' then
        acc <= (others => '0');
      elsif rising_edge(main_clk) then
        acc <= ('0'&acc(31 downto 0)) + ('0'&uart_conf.baud_gen_step);
      end if;
    end process;

    baud_clk <= acc(32);
end behavior;
