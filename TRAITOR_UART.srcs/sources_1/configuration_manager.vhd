----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes & L. Claudepierre
-- Target Devices: ArtiX7
-- Description:  Interface UART to read memory and send the results to the Host

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;

entity configuration_manager is
  port(clk:       in std_logic;
       reset:     in std_logic;
       
       --fifo from Uart PC rx (cross clock domains)
       din:         in byte;
       din_rdy:     in std_logic;
       din_read:    out std_logic;-- send read order

       --fifo to Uart PC tx (cross clock domains)
       dout:        out byte;
       dout_write:  out std_logic;-- send write order

      -- triggers
      trig_amplitude :  out std_logic_vector(7 downto 0);
      trig_confs:       out TrigConfs
       );
end configuration_manager;

architecture behavior of configuration_manager is

  component fifo_autoreader is
    port(clk:             in std_logic;
         reset:           in std_logic;
         enable:          in std_logic;
         fifo_empty:      in std_logic;
         fifo_data:       in std_logic_vector(7 downto 0);
         fifo_read_order: out std_logic;

         data:            out std_logic_vector(7 downto 0);
         data_en:         out std_logic
         );
  end component;

  -- protocel is [address(15 downto 0), payload_len(7 downto 0), data(8*payload_len-1 downto 0)]
  type COMMAND_FSM_STATE is (FUN, ADD, WRITE_DATA, READ_DATA, WAIT1, SEND_CHECKSUM); -- FUN is skipped

  signal data_byte: byte;
  signal data_en:   std_logic;

  signal dout_write_order: std_logic;

  signal fifo_empty: std_logic;

  signal command_state: COMMAND_FSM_STATE;
  signal mem: std_logic_vector(MAX_ADDRESS*8-1 downto 0);

  -- signal address: unsigned(7 downto 0);
  signal v_address: std_logic_vector(7 downto 0);
  signal v_state: std_logic_vector(2 downto 0);
  signal address: integer range 0 to 255;
  signal count_add: integer range 0 to 255;
  signal crw: std_logic; -- 0 read 1 write

  signal autoread_enable: std_logic;

  signal dout_buf, checksum: byte;


  begin

    fifo_empty <= not(din_rdy);
    dout <= dout_buf;
    dout_write <= dout_write_order;
    autoread_enable <= '1';
--    v_data_en(0)<=data_en;
--    v_crw(0)<=crw;
    -- Memory mapping

    --triggers confs
    gen_trig_confs: process(reset,mem)
begin
    trig_amplitude <= mem(LENGTH_AMPLITUDE*8-1 downto 0);
    for i in 0 to NB_TRIGGERS-1 loop
      trig_confs(i).trig_delay <= unsigned(mem((i*LENGTH_TRIG_CONFS + LENGTH_DELAY+LENGTH_AMPLITUDE)*8-1 downto (i*LENGTH_TRIG_CONFS+LENGTH_AMPLITUDE)*8));        
      trig_confs(i).trig_width <= unsigned(mem((i*LENGTH_TRIG_CONFS + LENGTH_DELAY+LENGTH_WIDTH+LENGTH_AMPLITUDE)*8-1 downto (i*LENGTH_TRIG_CONFS + LENGTH_DELAY+LENGTH_AMPLITUDE)*8));
    end loop;
    
    end process;

--v_count_add <= std_logic_vector(to_unsigned(count_add,4));
address <= to_integer(unsigned(v_address));

    -- mem operations
    read_fifo: fifo_autoreader
      port map(
        clk               => clk,
        reset             => reset,
        enable            => autoread_enable,
        fifo_empty        => fifo_empty,
        fifo_data         => din,
        fifo_read_order   => din_read,

        data              => data_byte,
        data_en           => data_en
      );

    update_mem: process(clk, reset)
    begin
      if reset = '1' then
        mem <= (others => '0');
      elsif rising_edge(clk) then
        if command_state = READ_DATA and data_en = '1' then
          mem(address*8+7 downto address*8) <= data_byte;
        end if;
      end if;
    end process;

    update_checksum: process(clk, reset)
    begin
      if reset = '1' then
        checksum <= X"00";
      elsif rising_edge(clk) then
        if command_state = FUN then
          checksum <= X"00";
        elsif command_state = WRITE_DATA or command_state = READ_DATA then
          checksum <= v_address(7 downto 0);-- checksum xor data_byte;
        end if;
      end if;
    end process;

    write_back: process(clk, reset)--write data to host
    begin
      if reset = '1' then
        dout_buf <= X"00";
        dout_write_order <= '0';
      elsif rising_edge(clk) then
        if command_state = SEND_CHECKSUM then
          dout_buf <= checksum;
          dout_write_order <= '1';
        elsif command_state = WRITE_DATA then
          dout_buf <= mem(address*8+7 downto address*8);
          dout_write_order <= '1';
        else
          dout_write_order <= '0';
        end if;
      end if;
    end process;

    add_set: process(clk, reset)--write data to host
    begin
      if reset = '1' then
        v_address <= x"00";
      elsif rising_edge(clk) then
        if command_state = ADD and data_en = '1' then
            v_address(7 downto 0) <= data_byte; -- SIZE ADRESS = 2 BYTES
        end if;
      end if;
    end process;


    fsm_command: process(clk, reset,count_add)
      begin
        if reset = '1' then
          command_state <= FUN;
          crw <= '0';
        elsif rising_edge(clk) then
          case command_state is
            when FUN =>
                v_state<="000";
              if data_en = '1' then -- read function
                if data_byte = X"00" then
                  crw <= '0';
                  command_state <= ADD;
                elsif data_byte = X"01" then
                  crw <= '1';
                  command_state <= ADD;
                else -- test
                  crw <= '0';
                  command_state <= SEND_CHECKSUM;
                end if;
              else
                command_state <= FUN;
              end if;


            when ADD =>
                v_state<="001";
              if data_en = '1' then --and count_add>=1 then
                if crw = '1' then --write mode expect data value
                  command_state <= READ_DATA;
                else
                  command_state <= WRITE_DATA;
                end if;
              else
                command_state <= ADD;
              end if;

            when READ_DATA => -- read data from host and write to mem
              v_state<="010";
              if data_en = '1' then
                command_state <= SEND_CHECKSUM;
              else
                command_state <= READ_DATA;
              end if;

            when WRITE_DATA => -- write data to host
              v_state<="011";
              command_state <= WAIT1;

            when WAIT1 =>
              v_state<="100";
              command_state <= SEND_CHECKSUM;


            when SEND_CHECKSUM =>
                v_state<="101";
              command_state <= FUN;

          end case;
        end if;
    end process;

end behavior;
