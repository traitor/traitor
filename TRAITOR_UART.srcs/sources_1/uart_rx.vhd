----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description: 

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

entity uart_rx is
  port(baud_clk:  in std_logic;
       reset:     in std_logic;
       uart_conf: in UartConf;
       rx:        in std_logic;--rx wire
       rx_data:   out std_logic_vector(7 downto 0);--data byte read
       rx_rdy:    out std_logic;
       error_flag:     out std_logic;
       tick_o:         out std_logic
       );
end uart_rx;

architecture behavior of uart_rx is

  constant PATT_SIZE: Integer := 3;
  type FSM_STATE is (START1, START2, DATA0, DATA1, DATA2, DATA3, DATA4, DATA5, DATA6, DATA7, PARITY, STOP1, STOP2);

  signal state: FSM_STATE;
  signal tick: std_logic; -- RE for each bit

  signal filter_rx: std_logic_vector(PATT_SIZE-1 downto 0);--filter rx signal to detect start
  signal baud_clk_count: unsigned(3 downto 0);--OVERSAMPLING = 16
  signal baud_clk_tick_value: unsigned(3 downto 0);-- tick when this value is reached

  signal start_flag: std_logic;
  signal running: std_logic;
  signal start_pattern: std_logic_vector(PATT_SIZE-1 downto 0);

  signal data_parity: std_logic;
  signal parity_bit: std_logic;
  signal datafill: std_logic;
  signal rx_data_buf: std_logic_vector(7 downto 0);
  signal end_flag: std_logic;
  signal error: std_logic;

  begin
  
    error_flag <= error;
  
    --filter rx
    filter: process(reset, baud_clk)
      begin
        if reset = '1' then
          filter_rx <= (0 => not uart_conf.idle_polarity,others => uart_conf.idle_polarity);
        elsif rising_edge(baud_clk) then
          -- filter_rx <=  (1 => filter_rx(0), 0 => rx);
          filter_rx <= filter_rx(PATT_SIZE-2 downto 0) & rx;
        end if;
    end process;

    --detect start bit
    -- start_pattern <= (1 => not uart_conf.idle_polarity, 0 => not uart_conf.idle_polarity);
    start_pattern(PATT_SIZE-1 downto 0) <= (others => not uart_conf.idle_polarity);
    detect_start: process(reset, baud_clk)
      begin
        if reset = '1' then
          start_flag <= '0';
        elsif rising_edge(baud_clk) then
          -- if state = START1 and filter_rx = start_pattern then
          if start_flag = '0' and running = '0' and state = START1 and filter_rx = start_pattern then
            start_flag <= '1';
          else
            start_flag <= '0';
          end if;
        end if;
    end process;

    -- baud clk counter
    count_baud_clk: process(reset, baud_clk)
      begin
        if reset = '1' then
          baud_clk_count <= (others => '0');
        elsif rising_edge(baud_clk) then
          if running = '1' then
            baud_clk_count <= baud_clk_count + 1;
          end if;
        end if;
    end process;

    baud_clk_tick_value_update: process(reset, baud_clk)
      begin
        if reset = '1' then
          baud_clk_tick_value <= X"0";
        elsif rising_edge(baud_clk) then
          if start_flag = '1' then
            baud_clk_tick_value <= baud_clk_count + 1;
          end if;
        end if;
    end process;

    running_up: process(reset, baud_clk)
      begin
        if reset = '1' then
          running <= '0';
        elsif rising_edge(baud_clk) then
          if start_flag = '1' then
            running <= '1';
          elsif end_flag = '1' then
            running <= '0';
          end if;
        end if;
    end process;

    tick_o <= tick;

    -- tick generation
    tick_gen: process(reset, baud_clk)
      begin
        if reset = '1' then
          tick <= '0';
        elsif rising_edge(baud_clk) then
          if baud_clk_count = baud_clk_tick_value and running = '1' then
            tick <= '1';
          else
            tick <= '0';
          end if;
        end if;
    end process;

    rx_data <= rx_data_buf;
    datafill_proc: process(reset, baud_clk)
      begin
        if reset = '1' then
          rx_data_buf <= (others => '0');
          data_parity <= '0';
        elsif rising_edge(baud_clk) then
          if tick = '1' then
            if datafill = '1' then
              rx_data_buf <= rx&rx_data_buf(7 downto 1);
              data_parity <= data_parity xor rx;
            elsif state = START1 then
              data_parity <= '0';
            end if;
          end if;
        end if;
    end process;

    check_parity: process(reset, baud_clk)
    begin
      if reset = '1' then
        error <= '0';
      elsif rising_edge(baud_clk) then
        if tick = '1' then
          if state = STOP1 then
            if uart_conf.parity = Even  then
              error <= data_parity xor parity_bit;
            elsif uart_conf.parity = Odd then
              error <= not(data_parity xor parity_bit);
            else
              error <= '0';
            end if;
          elsif state = START1 then
            error <= '0';
          end if;
        end if;
      end if;
    end process;

    -- FSM state transition
    transition: process(reset, baud_clk)
      begin
        if reset = '1' then
          state <= START1;
          rx_rdy <= '0';
          datafill <= '0';
          end_flag <= '0';
          parity_bit <= '0';
        elsif rising_edge(baud_clk) then
          trans: case state is

            when START1 => --START1 wait for start bit(s)
              rx_rdy <= '0';
              end_flag <= '0';
              parity_bit <= '0';
              if tick = '1' then
                if running = '1' then -- first START bit has happened
                  if uart_conf.start_bits = One then
                    state <= DATA0;
                    datafill <= '1';
                  else
                    state <= START2;
                  end if;
                else
                  state <= START1;
                end if;
              else
                state <= START1;
              end if;

            when START2 =>
              if tick = '1' then
                state <= DATA0;
                datafill <= '1';
              else
                state <= START2;
              end if;

            when DATA0 =>
              if tick = '1' then
                state <= DATA1;
              else
                state <= DATA0;
              end if;

            when DATA1 =>
              if tick = '1' then
                state <= DATA2;
              else
                state <= DATA1;
              end if;

            when DATA2 =>
              if tick = '1' then
                state <= DATA3;
              else
                state <= DATA2;
              end if;

            when DATA3 =>
              if tick = '1' then
                state <= DATA4;
              else
                state <= DATA3;
              end if;

            when DATA4 =>
              if tick = '1' then
                state <= DATA5;
              else
                state <= DATA4;
              end if;

            when DATA5 =>
              if tick = '1' then
                state <= DATA6;
              else
                state <= DATA5;
              end if;

            when DATA6 =>
              if tick = '1' then
                  if uart_conf.data_bits = Seven then
                    datafill <= '0';
                    if uart_conf.parity = None then
                      state <= STOP1;
                    else
                      state <= PARITY;
                    end if;
                  else
                    state <= DATA7;
                  end if;
                else
                  state <= DATA6;
                end if;

            when DATA7 =>
              if tick = '1' then
                datafill <= '0';
                if uart_conf.parity = None then
                  state <= STOP1;
                else
                  state <= PARITY;
                end if;
              else
                state <= DATA7;
              end if;

            when PARITY =>
              if tick = '1' then
                parity_bit <= rx;
                state <= STOP1;
              else
                state <= PARITY;
              end if;

            when STOP1 =>
              if tick = '1' then
                if uart_conf.stop_bits = One then
                  state <= START1;
                  end_flag <= '1';
                  if error = '0' then
                    rx_rdy <= '1';
                  end if;
                else
                  state <= STOP2;
                end if;
              else
                state <= STOP1;
              end if;

            when STOP2 =>
              if tick = '1' then
                state <= START1;
                end_flag <= '1';
                if error = '0' then
                  rx_rdy <= '1';
                end if;
              else
                state <= STOP2;
              end if;
          end case;
        end if;
    end process;

end behavior;
