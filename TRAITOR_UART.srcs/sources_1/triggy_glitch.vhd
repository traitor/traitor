----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: L. Claudepierre
-- Target Devices: ArtiX7
-- Description:  Wrapper of Glitch generation and Memory/UART communication

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;


entity triggy_glitch is
  Port (
        --//-------------- triggy_top -------------------//--
        clk100:          in std_logic;
        rst:           in std_logic;
        Dswitch : in STD_LOGIC_VECTOR(1 downto 0);
        stm_trig: in STD_LOGIC;
        
        led     : out STD_LOGIC_VECTOR (15 downto 0);
        
        clk_init : out STD_LOGIC;
        clk_dphase : out STD_LOGIC;
        clk_XOR_out : out STD_LOGIC;
        Fault_trig_out : out STD_LOGIC;
        clk_fault_out : out STD_LOGIC;
        clk_fault_out2 : out STD_LOGIC;

       -- PC Uart
        rx_from_pc:      in std_logic;
        tx_to_pc:        out std_logic
        
         );
end triggy_glitch;

architecture Behavioral of triggy_glitch is

component triggy_top IS
    PORT (
        clk_in:          in std_logic;
        rst:           in std_logic;
        
        -- PC Uart
        rx_from_pc:      in std_logic;
        tx_to_pc:        out std_logic;
     
       
        trig_delay_out: out trig_delays;
        trig_width_out : out  trig_widths;
        trig_amplitude_out : out  STD_LOGIC_VECTOR(7 downto 0)
    );
  end component;
  
component mmcm_reset IS
  PORT (
        clk_100 : in STD_LOGIC;
        Dswitch : in STD_LOGIC_VECTOR(1 downto 0);
        stm_trig: in STD_LOGIC;
        rst:           in std_logic;
        
        led     : out STD_LOGIC_VECTOR (15 downto 0);
        
        clk_init : out STD_LOGIC;
        clk_dphase : out STD_LOGIC;
        clk_XOR_out : out STD_LOGIC;
        Fault_trig_out : out STD_LOGIC;
        clk_fault_out : out STD_LOGIC;
        clk_fault_out2 : out STD_LOGIC;
   
        trig_delay_in: in trig_delays;
        trig_width_in : in  trig_widths;
        trig_amplitude_in : in  STD_LOGIC_VECTOR(7 downto 0)
  );
end component;

--    signal memoire : std_logic_vector(MAX_ADDRESS*8-1 downto 0);
    signal trig_delay: trig_delays;
    signal trig_width : trig_widths;
    signal trig_amplitude : STD_LOGIC_VECTOR(7 downto 0);
begin

trig_uart: triggy_top 
    port map (
        clk_in => clk100,
        rst => rst,
        
        -- PC Uart
        rx_from_pc=> rx_from_pc,
        tx_to_pc => tx_to_pc,
        
        --trigs
        trig_delay_out => trig_delay,
        trig_width_out => trig_width,
        trig_amplitude_out => trig_amplitude
    );


glitch_uart: mmcm_reset
    port map (
        clk_100 => clk100,
        Dswitch => Dswitch,
        stm_trig => stm_trig,
        
        rst => rst,
        led => led,
        clk_init => clk_init,
        clk_dphase => clk_dphase,
        clk_XOR_out => clk_XOR_out,
        Fault_trig_out => Fault_trig_out,
        clk_fault_out => clk_fault_out,
        clk_fault_out2 => clk_fault_out2,
        
        trig_delay_in => trig_delay,
        trig_width_in => trig_width,
        trig_amplitude_in => trig_amplitude
    );
end Behavioral;
