----------------------------------------------------------------------------------

-- TRAITOR
--Copyright (C)  2019 - INRIA

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes
-- Target Devices: ArtiX7
-- Description: constant and data for the glitch and memorisation

--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.common.all;

package triggy_common is



  -- config addresses
  constant STATE_ADD: Integer := 0;--size 1
  constant STATE_SIZE: Integer := 1;

  constant TRIGGERS_ADD: Integer := STATE_ADD+STATE_SIZE;--1
  constant TRIGGER_CONF_SIZE: Integer := 9;-- 17;
  constant NB_TRIGGERS: Integer := 64;

  -- size of config
  constant LENGTH_DELAY: Integer := 4;
  constant LENGTH_WIDTH: Integer := 4;
  constant LENGTH_AMPLITUDE: Integer := 1;
  constant LENGTH_TRIG_CONFS: Integer := LENGTH_DELAY + LENGTH_WIDTH;
  constant MAX_ADDRESS: Integer := LENGTH_AMPLITUDE+LENGTH_TRIG_CONFS*NB_TRIGGERS;-- 64 //--96

  type TrigConf is
    record
      trig_delay:  unsigned(LENGTH_DELAY*8-1 downto 0);--wait before trig in and rising edge
      trig_width:  unsigned(LENGTH_WIDTH*8-1 downto 0);--trig up width
  end record;

  type TrigConfs is array (0 to NB_TRIGGERS-1) of TrigConf;

  type trig_delays is array (0 to NB_TRIGGERS-1) of unsigned(LENGTH_DELAY*8-1 downto 0);
  type trig_widths is array (0 to NB_TRIGGERS-1) of unsigned(LENGTH_WIDTH*8-1 downto 0);

end triggy_common;
