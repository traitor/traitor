----------------------------------------------------------------------------------

-- TRAITOR 
--Copyright (C)  2019 - INRIA 

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU Affero General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU Affero General Public License for more details.

--You should have received a copy of the GNU Affero General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Author: R. Lashermes & L.Claudepierre
-- Target Devices: ArtiX7
-- Description:  Wrapper of Memory and UART communication

-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.triggy_common.all;
use work.common.all;
use work.all;

library UNISIM;
use UNISIM.VComponents.all;

entity triggy_top is
  port(
        clk_in:          in std_logic;
        rst:           in std_logic;
        trig_delay_out: out trig_delays;
        trig_width_out : out  trig_widths;
        trig_amplitude_out : out  STD_LOGIC_VECTOR(7 downto 0);
        -- PC Uart
        rx_from_pc:      in std_logic;
        tx_to_pc:        out std_logic
       );
end triggy_top;

architecture behavior of triggy_top is


  component fifo IS
    PORT (
      clk : IN STD_LOGIC;
      rst : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      wr_rst_busy : OUT STD_LOGIC;
      rd_en : IN STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
    );
  end component;

  component uart is
    port(clk:  in std_logic;
         reset:     in std_logic;
         uart_conf: in UartConf;

         rx:        in std_logic;--rx wire
         tx:        out std_logic;--tx wire

         tx_data:   in std_logic_vector(7 downto 0);--data byte to write
         rx_data:   out std_logic_vector(7 downto 0);--data byte read

         rx_rdy:    out std_logic;--a byte is ready to be read
         tx_rdy:    out std_logic;--a byte can be written
         tx_start:  in std_logic;--write a byte

         error_flag: out std_logic;

         baud_clk_o: out std_logic;
         tick_o:    out std_logic
         );
  end component;

  component configuration_manager is
    port(clk:  in std_logic;
         reset:     in std_logic;
         --fifo from Uart PC rx (cross clock domains)
         din:         in byte;
         din_rdy:     in std_logic;
         din_read:    out std_logic;-- send read order

         --fifo to Uart PC tx (cross clock domains)
         dout:        out byte;
         dout_write:  out std_logic;-- send write order
         
        -- triggers
        trig_amplitude:   out std_logic_vector(7 downto 0);
        trig_confs:       out TrigConfs
         );
  end component;


  component pulser is
    port(clk_in:       in std_logic;
         clk_out:       in std_logic;
         reset:     in std_logic;
         pulse_in:  in std_logic;
         pulse_out: out std_logic
         );
  end component;

  component fifo_autoreader is
    port(clk:             in std_logic;
         reset:           in std_logic;
         enable:          in std_logic;
         fifo_empty:      in std_logic;
         fifo_data:       in std_logic_vector(7 downto 0);
         fifo_read_order: out std_logic;

         data:            out std_logic_vector(7 downto 0);
         data_en:         out std_logic
         );
  end component;


  signal clk: std_logic;

  --PC uart
  signal pc_tx_data, pc_rx_data: std_logic_vector(7 downto 0);
  signal pc_error_flag: std_logic;
  signal tx_to_pc_buf: std_logic;
  signal wr_rst_busy_01: std_logic;
  signal wr_rst_busy_02: std_logic;
  signal rd_rst_busy_01: std_logic;
  signal rd_rst_busy_02: std_logic;
  signal uart_conf_s: UartConf;
  signal delay: u32;
  signal start_delay: std_logic;
  signal clkfb                  : STD_LOGIC; --std_logic := '0';

  -- fifo rx
  signal fifo_rx_dout, fifo_rx_din: std_logic_vector(7 downto 0);
  signal fifo_rx_available, fifo_rx_empty, fifo_rx_read_order: std_logic;

  -- fifo tx
  signal fifo_tx_dout, fifo_tx_din: std_logic_vector(7 downto 0);
  signal fifo_tx_empty, fifo_tx_write_order, fifo_tx_read, fifo_tx_enable, fifo_tx_enlock: std_logic;

  signal pc_rx_rdy, pulse_rx_rdy, pc_tx_rdy, pc_tx_start: std_logic;
  signal baud_clk: std_logic;

  -- triggers
  signal trig_confs: TrigConfs;

  signal reset: std_logic;
  
  begin
--  clk <= clk_in;


buf_rst: process(clk_in)
begin
    if rising_edge(clk_in) then
        reset<=rst;
    end if;
end process;   
  
uart_conf_s<= (idle_polarity => '1',
               baud_gen_step => X"04B7F5A5",-- To adapt with clk period -> 115200 bauds
               start_bits => One,
               stop_bits => One,
               parity => None,
               data_bits => Eight);
                                    
remplissage_trigconf : process(rst,trig_confs , clk_in)
begin
        for i in 0 to NB_TRIGGERS-1 loop
            trig_delay_out(i) <= trig_confs(i).trig_delay;
            trig_width_out(i) <= trig_confs(i).trig_width;
        end loop;
end process;


--     clock generation
    pll_clocking : PLLE2_BASE
   generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLKFBOUT_MULT      => 10,
      CLKFBOUT_PHASE     => 0.0,
      CLKIN1_PERIOD      => 10.0,

      -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
      CLKOUT0_DIVIDE     => 10,  CLKOUT1_DIVIDE     => 125, CLKOUT2_DIVIDE      => 125, 
      CLKOUT3_DIVIDE     => 125,  CLKOUT4_DIVIDE     => 125, CLKOUT5_DIVIDE      => 125,

      -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
      CLKOUT0_DUTY_CYCLE => 0.5, CLKOUT1_DUTY_CYCLE => 0.5, CLKOUT2_DUTY_CYCLE => 0.5,
      CLKOUT3_DUTY_CYCLE => 0.5, CLKOUT4_DUTY_CYCLE => 0.5, CLKOUT5_DUTY_CYCLE => 0.5,

      -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
      CLKOUT0_PHASE      =>    0.0, CLKOUT1_PHASE      => -354.6 , CLKOUT2_PHASE      => -352.4,
      CLKOUT3_PHASE      => -351.7, CLKOUT4_PHASE      => -350.6, CLKOUT5_PHASE      => -349.2,

      DIVCLK_DIVIDE      => 1,
      REF_JITTER1        => 0.0,
      STARTUP_WAIT       => "FALSE"
   )
   port map (
      CLKIN1   => clk_in,
      CLKOUT0  => clk,   CLKOUT1 => open,  CLKOUT2 => open,  
      CLKOUT3  => open, CLKOUT4 => open,  CLKOUT5 => open,
      LOCKED   => open,
      PWRDWN   => '0', 
      RST      => '0',
      CLKFBOUT => clkfb,
      CLKFBIN  => clkfb
   );



    -- signal to write to fifo form uart
    pc_uart_pulser: pulser
      port map(
        clk_in      => baud_clk,
        clk_out     => clk,
        reset       => reset,
        pulse_in    => pc_rx_rdy,
        pulse_out   => pulse_rx_rdy
      );

    -- UART between Triggy and host PC
    tx_to_pc <= tx_to_pc_buf;
    pc_uart: uart
      port map(
        clk           => clk,
        reset         => reset,

        uart_conf     => uart_conf_s,

        rx            => rx_from_pc,
        tx            => tx_to_pc_buf,

        tx_data       => pc_tx_data,
        rx_data       => pc_rx_data,

        rx_rdy        => pc_rx_rdy,
        tx_rdy        => pc_tx_rdy,
        tx_start      => pc_tx_start,
        error_flag    => pc_error_flag,

        baud_clk_o    => baud_clk,
--        tick_o        => debug(0)
         tick_o        => open
      );

-- BUFFER between PC Uart and config_manager: fifo + autoreaders

    pc_uart_rx_fifo: fifo
        port map(
        rst           => reset,
        clk           => clk,
        din           => pc_rx_data,
        wr_en         => pulse_rx_rdy,
        rd_en         => fifo_rx_read_order,
        dout          => fifo_rx_dout,
        rd_rst_busy   => rd_rst_busy_01,
        wr_rst_busy   => wr_rst_busy_01,
        full          => open,
        empty         => fifo_rx_empty
        );

      fifo_rx_available <= not(fifo_rx_empty);

      pc_uart_tx_fifo: fifo
        port map(
        rst           => reset,
        clk           => clk,
        din           => fifo_tx_din,
        wr_en         => fifo_tx_write_order,
        rd_en         => fifo_tx_read,
        dout          => fifo_tx_dout,
        rd_rst_busy   => rd_rst_busy_02,
        wr_rst_busy   => wr_rst_busy_02,
        full          => open,
        empty         => fifo_tx_empty
        );


      process(clk, reset)
      begin
        if reset = '1' then
          fifo_tx_enable <= '1';
          fifo_tx_enlock <= '0';
          delay <= X"00000000";
          start_delay <= '0';
        elsif rising_edge(clk) then
          if fifo_tx_read = '1' then
            fifo_tx_enable <= '0';
            fifo_tx_enlock <= '1';
            start_delay <= '0';
          elsif pc_tx_rdy = '0' then
            fifo_tx_enlock <= '0';
            start_delay <= '1';
            delay <= X"00000100";
          elsif fifo_tx_enlock = '0' and pc_tx_rdy = '1' then
            if start_delay = '1' then
              if delay = X"00000000" then
                fifo_tx_enable <= '1';
                start_delay <= '0';
              else
                delay <= delay - 1;
              end if;
            end if;
          end if;
        end if;
      end process;

      fifo_to_tx: fifo_autoreader
        port map(
          clk               => clk,
          reset             => reset,
          enable            => fifo_tx_enable,-- pc_tx_rdy,
          fifo_empty        => fifo_tx_empty,
          fifo_data         => fifo_tx_dout,
          fifo_read_order   => fifo_tx_read,

          data              => pc_tx_data,
          data_en           => pc_tx_start
        );

      -- where the Triggy configuration is stored

      config_manager: configuration_manager
        port map(
          clk      => clk,
          reset         => reset,
          din           => fifo_rx_dout,
          din_rdy       => fifo_rx_available,
          din_read      => fifo_rx_read_order,

          --fifo to Uart PC tx (cross clock domains)
          dout          => fifo_tx_din,
          dout_write    => fifo_tx_write_order,-- send write order

          -- triggers
          trig_amplitude      => trig_amplitude_out,
          trig_confs      => trig_confs

          );
end behavior;
