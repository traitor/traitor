# About this repository

- This is the source code of TRAITOR (**TRA**nsportable gl**I**tch a**T**tack
  platf**OR**m), a FPGA platform used for fault injection, specifically clock
  glitch fault injection. More details in our paper [1].
- VHDL has been created with Vivado version 2018.3. You should synthetized the
  code with this specific version, or with another one that has been tested :
  2019.2. Both versions produce a working synthesis. Results with other
  versions is unknown.
- FPGA target is the Arty A7-35T [2]
- Attacked device in the paper is the STM32F100RB [3], *a.k.a.*
  STM32F1Discovery-VL
- If you use TRAITOR, please cite our paper [1].

# Usage

To configure the FPGA, use any software that can communicate on serial port and
use this configuration:
- Baud rate : 115200
- Bits : 8
- Stopbits : 1
- Parity : None
- Flow control : None

Here's the memory layout of the FPGA. Address `0x00` is used for a specific
parameter called **amplitude** (see paper). Each glitch is coded on 8 bytes: 4
bytes for the **delay**, 4 bytes for the **width**. User can encode up to 31
glitches.

|  Variable |    Adresse    |      Value     |   |   |
|:---------:|:-------------:|:--------------:|---|---|
| Amplitude |     `0x00`    |  `[0xE0-0xF0]` |   |   |
|  Delay 1  | `[0x01-0x04]` | User defined   |   |   |
|  Width 1  | `[0x05-0x08]` | User defined   |   |   |
|  Delay 2  | `[0x09-0x0C]` | User defined   |   |   |
|  Width 2  | `[0x0D-0x10]` | Used defined   |   |   |
|    ...    |     ...       |      ...       |   |   |

# References

* [1] Ludovic Claudepierre, Pierre-Yves Péneau, Damien Hardy, Erven Rohou.
  TRAITOR: A Low-Cost Evaluation Platform for Multifault Injection. *In
  International Symposium on Advanced Security on Software and Systems (ASSS)*,
  ACM 2021
* [2] https://www.xilinx.com/products/boards-and-kits/1-elhaap.html
* [3] https://www.st.com/en/microcontrollers-microprocessors/stm32f100rb.html
